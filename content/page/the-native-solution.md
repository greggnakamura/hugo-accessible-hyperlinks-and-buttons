+++
date        = "2017-05-17T11:27:27-04:00"
title       = "The Native Solution"
description = "HTML5 Elements are already accessible"

+++
Just to reiterate what was mentioned in the Intro:
<blockquote>
**`<button>` and `<a>` are interactive controls, focusable elements, can be operated by your mouse and keyboard and are accessible.**
</blockquote>

---

## `<button>`
*`onclick` event will fire for mouse clicks and when pressing `Space` or `Enter`, while the `<button>` has focus.*
``` html
<!-- original -->
<div class="button">Click here</div>

<!-- a11y fix -->
<div role="button" class="button" tabindex="0">Click here</div>

<!-- HTML5 button element -->
<button class="button">Click here</button>
```
<button class="button">Click here</button>

---

## `<a>`
*`onclick` event will fire for mouse clicks and when pressing `Enter`, while the `<a>` has focus.*
``` html
<!-- original -->
<span class="link">Google</span>

<!-- a11y fix -->
<span role="link" class="link" tabindex="0">Google</span>

<!-- HTML5 button element -->
<a href="http://www.google.com" class="link">Google</a>
```
<a href="http://www.google.com" class="link">Google</a>

---

Take away:

- Use native element when working with `<button>` and `<a>`
- You get a bunch of stuff free
- Avoid recreating the wheel *(element, functionality and accessibility)*


<!-- scripts -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
<script>
  (function ($) {

    $('.post-content button').on('click', function () {
      alert('button clicked');
    });

    $('.post-content a').on('click', function (e) {
      e.preventDefault();
      alert('link clicked');
    });

  })(jQuery);
</script>