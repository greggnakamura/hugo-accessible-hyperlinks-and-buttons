+++
date        = "2017-05-16T11:27:27-04:00"
title       = "Accessible Hyperlinks and Buttons"
description = ""

+++

**`<button>` and `<a>` are interactive controls, focusable elements, can be operated by your mouse and keyboard and are accessible.**

- `<button>`
  - `onclick` event will fire on **ENTER** and **SPACE** keys
  - `role="button"`
- `<a>`
  - `onclick` event will fire on **ENTER** key
  - `role="link"`

---

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">Accessibility Pro Tip: Links vs. Buttons - links navigate, buttons activate <a href="https://twitter.com/hashtag/micdrop?src=hash">#micdrop</a> <a href="https://twitter.com/hashtag/a11y?src=hash">#a11y</a> <a href="https://twitter.com/hashtag/accessibility?src=hash">#accessibility</a> <a href="https://twitter.com/hashtag/webdev?src=hash">#webdev</a> <a href="https://twitter.com/hashtag/appdev?src=hash">#appdev</a> <a href="https://twitter.com/hashtag/ux?src=hash">#ux</a> <a href="https://twitter.com/hashtag/html?src=hash">#html</a></p>&mdash; A11YChi Meetup (@A11YChi) <a href="https://twitter.com/A11YChi/status/860153367234060288">May 4, 2017</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

`<a>` created as a *"button"* (i.e. `<a class="btn">`), while visually appearing as a button, is textually announced as a link, potentially confusing a user who relies on assistive technologies.

<a class="button" href="#">This is a "button"</a>
