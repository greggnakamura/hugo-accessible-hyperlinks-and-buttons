+++
date        = "2017-05-16T11:27:27-04:00"
title       = "Resolved"
description = ""

+++

### `<div>`
``` html
<!-- original -->
<div class="button">Click here</div>

<!-- a11y fix -->
<div role="button" class="button" tabindex="0">Click here</div>
```
<div role="button" class="button" tabindex="0">Click here</div>

- added `tabindex`
  - Allows element to receive keyboard focus
  - Element is inserted into the tab order based on its location in the source code
- added `role="button"`
  - Makes element appear as a `button` to a screen reader
- Screen Reader now announces element as: `click here, button`

---

### `<span>`
``` html
<!-- original -->
<span class="link">Google</span>

<!-- a11y fix -->
<span role="link" class="link" tabindex="0">Google</span>
```
<span role="link" class="link" tabindex="0">Google</span>

- added `tabindex`
- added `role="link"`
- Screen Reader now announces as: `link, Google`

<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
<script>
  (function ($) {
    $('.post-content .button')
      .on('click', function () {
        alert('button clicked with mouse');
      })
      .on('keydown', function (e) {
        if (e.which === 13 || e.which === 32) {
          alert('button clicked with keyboard (' + e.which + ')');
        }
      });

    $('.post-content .link')
      .on('click', function () {
        alert('link clicked with mouse');
      })
      .on('keydown', function (e) {
        if (e.which === 13) {
          alert('link clicked with keyboard');
        }
      });

  })(jQuery);
</script>