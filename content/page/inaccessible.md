+++
date        = "2017-05-16T11:27:27-04:00"
title       = "Inaccessible Examples"
description = "Button and link created with non-native elements"

+++
## `<div>` and `<span>`
- These are generic containers. Used to group elements for styling purposes
- Inherently not accessible

---

<!-- inaccessible code -->
### `<div>` used to create button
``` html
<div class="button">Click here</div>
```
<div class="button">Click here</div>

- not announced as a `<button>`
- announced as *"click here"* by screenreader
- not focusable, so can't interact with keyboard
- Developer Tools:
  - Chrome: `Role: Div`
  - Safari Inspector: `Role: No exact ARIA role match`

---
### `<span>` used as a link

``` html
<span class="link">Google</span>
```
<span class="link">Google</span>

- not announced as a `link`
- announced as "google" by screenreader
- Developer Tools:
  - Chrome: `Accessibility node is not exposed`
  - Safari Inspector: `Role: group (default)`

---

**⚠️  Neither will appear as and error or warning in automated testing tools !!**

[Make these things accessible!! <i class="fa fa-long-arrow-right" aria-hidden="true"></i>](/page/resolved/)

<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
<script>
  (function ($) {
    $('.post-content .button')
      .on('click', function () {
        alert('button clicked with mouse');
      })
      .on('keydown', function (e) {
        if (e.which === 13 || e.which === 32) {
          alert('button clicked with keyboard');
        }
      });

    $('.post-content .link')
      .on('click', function () {
        alert('link clicked with mouse');
      })
      .on('keydown', function (e) {
        if (e.which === 13) {
          alert('link clicked with keyboard');
        }
      });

  })(jQuery);
</script>