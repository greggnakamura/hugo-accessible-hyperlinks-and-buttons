# Accessible Hyperlinks and Buttons

Walkthough of `<a>` and `<button>`

- Introduction
- Inaccessible examples
- Resolving inaccessible examples
- Native Solution using core HTML5 elements

Built with: [Hugo - Static Website Engine](https://gohugo.io/)